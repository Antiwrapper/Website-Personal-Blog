jQuery.fn.center = function () {
	this.css("position","absolute");
	this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
	                              $(window).scrollTop()) + "px");
	this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
	                               $(window).scrollLeft()) + "px");
	return this;
}

$('#donate_window').hide();
$('.window').center();

$('#donate').on('click', function() {
	$('#donate_window').fadeIn(1500);
});

$('#donate_window').on('click', function() {
	$('#donate_window').fadeOut(500);
})